﻿using School.Models;
using System.Collections.Generic;

namespace School.ViewModels
{
    public class DbViewModel
    {
        public List<Student> Students { get; set; }
        public List<Teacher> Teachers { get; set; }
        public List<Course> Courses { get; set; }
        public List<Subject> Subjects { get; set; }
        public int ActiveTeacherId { get; set; }
    }
}
