﻿$(document).ready(function () {
    $('#table-students tbody tr').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });

    $('#table-subjects tbody tr').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        window.location.href = routeAttr;        
    });

    $('#btn-add-student').click(function (e) {
        e.preventDefault();
        var routeAttr = $(this).attr("href");
        var firstName = $(".input-add-firstname").val();
        var lastName = $(".input-add-lastname").val();
        var city = $(".input-add-city").val();

        routeAttr = routeAttr
            .replace("jsFirstNameDummy", firstName)
            .replace("jsLastNameDummy", lastName)
            .replace("jsCityDummy", city);
        window.location.href = routeAttr;
    });

    $('#btn-add-subject').click(function (e) {
        e.preventDefault();
        var routeAttr = $(this).attr("href");
        var subject = $(".input-add-subject").val();

        routeAttr = routeAttr
            .replace("jsSubjectDummy", subject);
        window.location.href = routeAttr;
    });

    $('#btn-add-teacher-course').click(function (e) {
        e.preventDefault();
        var routeAttr = $(this).attr("href");
        var course = $("#input-add-course").val();
        var firstName = $("#input-add-firstname-teacher").val();
        var lastName = $("#input-add-lastname-teacher").val();
        var city = $("#input-add-city-teacher").val();

        routeAttr = routeAttr
            .replace("jsCourseDummy", course)
            .replace("jsFirstNameDummy", firstName)
            .replace("jsLastNameDummy", lastName)
            .replace("jsCityDummy", city);
        window.location.href = routeAttr;
    });
});

function setActiveTeacher(id) {
    $("#tr-courseid-" + id).addClass('active').siblings().removeClass('active');
    $("#tr-teacherid-" + id).addClass('active2').siblings().removeClass('active2');
};