﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using School.Data;
using School.Helpers;
using School.Models;
using School.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Controller = Microsoft.AspNetCore.Mvc.Controller;

namespace School.Controllers
{
    public class SchoolController : Controller
    {
        private readonly SchoolDbContext _schoolContext;
        private readonly ILogger<SchoolController> _logger;

        public SchoolController(ILogger<SchoolController> logger, SchoolDbContext schoolContext)
        {
            _logger = logger;
            _schoolContext = schoolContext;
        }

        public IActionResult School()
        {
            int activeTeacherId = _schoolContext.TempStore.FirstOrDefault().ActiveTeacherId;

            DbViewModel dbViewModel = SetDbViewModel(activeTeacherId, numberOfProperties: 5);


            return View(dbViewModel);
        }
        public IActionResult DisplayRelationsToTeacher(int idTeacher)
        {
            DbViewModel dbViewModel = SetDbViewModel(idTeacher, numberOfProperties: 5);

            TempStored tempStore = new TempStored
            {
                TempStoredId = ((int)TempRow.ActiveTeacherId),
                ActiveCourseId = idTeacher,
                ActiveTeacherId = idTeacher
            };

            _schoolContext.TempStore.RemoveRange(_schoolContext.TempStore);
            _schoolContext.TempStore.Add(tempStore);
            _schoolContext.SaveChanges();

            ViewBag.ActiveTeacherId = tempStore.ActiveTeacherId;
            ViewBag.Info = "Click on row to see relating data to (Course & Teacher)!";

            return View("School", dbViewModel);
        }
        public IActionResult DisplayRelationsToCourse(int idCourse)
        {
            DbViewModel dbViewModel = SetDbViewModel(idCourse, numberOfProperties: 5);

            TempStored tempStore = new TempStored
            {
                TempStoredId = ((int)TempRow.ActiveTeacherId),
                ActiveCourseId = idCourse,
                ActiveTeacherId = idCourse
            };

            _schoolContext.TempStore.RemoveRange(_schoolContext.TempStore);
            _schoolContext.TempStore.Add(tempStore);
            _schoolContext.SaveChanges();

            ViewBag.Info = "Display Students, Teacher & Subjects for a selected \"Course\"!";

            return View("School", dbViewModel);
        }
        public IActionResult AddStudent(string firstName, string lastName, string city)
        {
            int addStudentSucceded = 0, addCourseSucceded = 0;
            int activeCourseId = _schoolContext.TempStore.FirstOrDefault().ActiveCourseId;
            string courseName = _schoolContext.Courses.Find(activeCourseId).CourseName;
            bool hasFirstName = _schoolContext.Students.Any(c => c.FirstName == firstName) || firstName == null;
            bool hasLastName = _schoolContext.Students.Any(c => c.LastName == lastName || lastName == null) ;
            bool hasCity = _schoolContext.Students.Any(c => c.City == city) || city == null;

            if (!(hasFirstName && hasLastName && hasCity))
            {
                     var student = new Student
                {
                    FirstName = firstName,
                    LastName = lastName,
                    City = city
                };
            
                _schoolContext.Add(student);
                addStudentSucceded = _schoolContext.SaveChanges();
                
                StudentCourse studentCourse = new StudentCourse
                {
                    CourseId = activeCourseId,
                    StudentId = student.StudentId
                };
          
                _schoolContext.StudentsCourses.Add(studentCourse);
                addCourseSucceded = _schoolContext.SaveChanges();
            }

            DbViewModel dbViewModel = SetDbViewModel(activeCourseId, numberOfProperties: 5);

            string msgStudent = (firstName != null) ? "The student " + firstName + " " + lastName + ", " + city : "Student ";
            string msgStudentAdded = " is applied to the course " + courseName + "!";
            string msgStudentNotAdded = " was not added to the course " + courseName + ", first name, last name and city are required!";
            string msgStudentHasSucceded = (addStudentSucceded > 0) ? msgStudentAdded : msgStudentNotAdded;
            ViewBag.Info = msgStudent + msgStudentHasSucceded;

            return View("School", dbViewModel);
        }
        public IActionResult DeleteStudent(int studentId)
        {
            int succeded = 0;
            var student = _schoolContext.Students.Find(studentId);

            bool isStudentIdMoreThanZero = studentId != 0;

            if (isStudentIdMoreThanZero)
            {
                _schoolContext.Remove(student);
                succeded = _schoolContext.SaveChanges();
            }

            int idTeacher = _schoolContext.TempStore.FirstOrDefault().ActiveTeacherId;

            DbViewModel dbViewModel = SetDbViewModel(idTeacher, numberOfProperties: 5);

            ViewBag.Info = "DELETE Student:" + studentId + "from Course";
            ViewBag.Info = (succeded > 0) ? "The Student with studentId:" + studentId + " is removed" : "Could not remove Student with Id:" + studentId + ".";

            return View("School", dbViewModel);
        }
        public IActionResult AddCourseAndTeacher(string course, string firstName, string lastName, string city)
        {
            int succeded = 0, courseId = 0, teacherId = 0;

            var newCourse = new Course { CourseName = course };

            bool hasCourse = !_schoolContext.Courses.Any(c => c.CourseName == newCourse.CourseName);

            if (hasCourse)
            {
                _schoolContext.Add(newCourse);
                succeded = _schoolContext.SaveChanges();
            }

            courseId = _schoolContext.Courses.Max(co => co.CourseId);
            teacherId = _schoolContext.Teachers.Max(co => co.TeacherId);

            if (courseId != teacherId + 1)
            {
                if (courseId < teacherId + 1)
                {
                    int diff = teacherId - courseId;
                    courseId = _schoolContext.Courses.Max(co => co.CourseId) + diff;
                }
                else if (courseId > teacherId + 1)
                {
                    courseId = _schoolContext.Courses.Max(co => co.CourseId);
                }
            }

            var newTeacher = new Teacher
            {
                FirstName = firstName,
                LastName = lastName,
                City = city,
                CourseId = courseId
            };

            bool hasFirstName = !_schoolContext.Students.Any(c => c.FirstName == newTeacher.FirstName);
            bool hasLastName = !_schoolContext.Students.Any(c => c.LastName == newTeacher.LastName);

            if (hasFirstName && hasLastName)
            {
                _schoolContext.Add(newTeacher);
                succeded = _schoolContext.SaveChanges();
            }

            DbViewModel dbViewModel = SetDbViewModel(courseId, numberOfProperties: 4);

            ViewBag.Info = "Added Course And Teacher " + course;

            return View("School", dbViewModel);
        }
        public IActionResult DeleteCourse(int courseId)
        {
            int succeded = 0;
            var course = _schoolContext.Courses.Find(courseId);
            bool isCourseIdMoreThanZero = courseId > 0;

            if (isCourseIdMoreThanZero)
            {
                _schoolContext.Remove(course);
                succeded = _schoolContext.SaveChanges();
            }

            DbViewModel dbViewModel = SetDbViewModel(courseId, numberOfProperties: 4);

            ViewBag.Info = (succeded > 0) ? "The Course:" + course.CourseName + " is removed" : "Could not remove Course: " + course.CourseName + ".";

            return View("School", dbViewModel);
        }
        public IActionResult DeleteTeacher(int teacherId)
        {
            int succeded = 0;
            var teacher = _schoolContext.Teachers.Find(teacherId);
            bool isTeacherIdMoreThanZero = teacherId > 0;

            if (isTeacherIdMoreThanZero)
            {
                _schoolContext.Remove(teacher);
                succeded = _schoolContext.SaveChanges();
            }

            DbViewModel dbViewModel = SetDbViewModel(teacherId, numberOfProperties: 4);

            ViewBag.Info = (succeded > 0) ? "The Teacher: " + teacher.FirstName + " " + teacher.LastName + " " + teacher.City + " is removed" : "Could not remove Teacher: " + teacher.FirstName + " " + teacher.LastName + " " + teacher.City + ".";

            return View("School", dbViewModel);
        }
        public IActionResult AddSubject(string subject)
        {
            int succeded = 0, teacherId = 0;

            if (!_schoolContext.TempStore.Any())
            {
                ViewBag.Info = "Choose the course to which the subject should belong!";
            }
            else
            {
                teacherId = _schoolContext.TempStore.FirstOrDefault().ActiveTeacherId;

                var newSubject = new Subject
                {
                    SubjectName = subject,
                    CourseId = teacherId,
                };
                bool hasSubject = !_schoolContext.Students.Any(c => c.FirstName == newSubject.SubjectName);

                if (hasSubject)
                {
                    _schoolContext.Subjects.Add(newSubject);
                    succeded = _schoolContext.SaveChanges();
                }
                ViewBag.Info = (succeded > 0) ? "Added Subject " + subject + " to Course" : "Something went wrong, the subject was not applied to the database!";

            }

            DbViewModel dbViewModel = SetDbViewModel(teacherId, numberOfProperties: 4);

            return View("School", dbViewModel);
        }
        public IActionResult DeleteSubject(int subjectId)
        {
            int succeded = 0;

            var subject = _schoolContext.Subjects.Find(subjectId);

            bool isSubjectIdMoreThanZero = subjectId != 0;

            if (isSubjectIdMoreThanZero)
            {
                _schoolContext.Remove(subject);
                succeded = _schoolContext.SaveChanges();
            }

            var activeTeacherId = _schoolContext.TempStore.FirstOrDefault().ActiveTeacherId;

            DbViewModel dbViewModel = SetDbViewModel(activeTeacherId, numberOfProperties: 4);

            ViewBag.Info = (succeded > 0) ? "The Subject with subjectId:" + subjectId + " is removed" : "Could not remove Subject with Id:" + subjectId + ".";

            return View("School", dbViewModel);
        }
        public List<Student> GetStudentsInCourse(int id)
        {
            return _schoolContext.Students
                .OrderBy(c => c.FirstName)
                .SelectMany(c => c.StudentCourses)
                .Where(c => c.CourseId == id)
                .Select(s => s.Student)
                .ToList();
        }
        public List<Subject> GetSubjectsInCourse(int id)
        {
            return _schoolContext.Subjects
                .Where(c => c.CourseId == id)
                .OrderBy(c => c.Course.Teacher.FirstName)
                .ThenBy(c => c.SubjectName)
                .ToList();
        }
        public DbViewModel SetDbViewModel(int activeId, int numberOfProperties)
        {
            switch (numberOfProperties)
            {
                case 5:
                return new DbViewModel
                {
                    Students = GetStudentsInCourse(activeId),
                    Teachers = _schoolContext.Teachers.ToList(),
                    Courses = _schoolContext.Courses.ToList(),
                    Subjects = GetSubjectsInCourse(activeId),
                    ActiveTeacherId = activeId
                };
                case 4:
                    return new DbViewModel
                    {
                        Students = GetStudentsInCourse(activeId),
                        Teachers = _schoolContext.Teachers.ToList(),
                        Courses = _schoolContext.Courses.ToList(),
                        Subjects = GetSubjectsInCourse(activeId),
                    };
                default:        // Instead of Case 2
                    return new DbViewModel
                    {
                        Teachers = _schoolContext.Teachers.ToList(),
                        Courses = _schoolContext.Courses.ToList()
                    };
            }
        }
    }
}