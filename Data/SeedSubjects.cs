﻿using Microsoft.EntityFrameworkCore;
using School.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Data
{
    public class SeedSubjects
    {
        public ModelBuilder AddAssignment(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subject>().HasData(
                new Subject { SubjectId = 1, SubjectName = "C#", CourseId = 1 },
                new Subject { SubjectId = 2, SubjectName = "HTML, CSS", CourseId = 1 },
                new Subject { SubjectId = 3, SubjectName = "Javascript, Bootstrap", CourseId = 1 },
                new Subject { SubjectId = 4, SubjectName = "SQL", CourseId = 1 },
                new Subject { SubjectId = 5, SubjectName = "Entity Framework", CourseId = 1 },
                new Subject { SubjectId = 6, SubjectName = "Identity", CourseId = 1 },
                new Subject { SubjectId = 7, SubjectName = "React", CourseId = 1 },
                new Subject { SubjectId = 8, SubjectName = "Azure", CourseId = 1 },
                new Subject { SubjectId = 9, SubjectName = "Macrame", CourseId = 2 },
                new Subject { SubjectId = 10, SubjectName = "Knyppling", CourseId = 2 },
                new Subject { SubjectId = 11, SubjectName = "Lek med papper", CourseId = 2 },
                new Subject { SubjectId = 12, SubjectName = "Glassyr", CourseId = 2 },
                new Subject { SubjectId = 13, SubjectName = "Minneshantering", CourseId = 3 },
                new Subject { SubjectId = 14, SubjectName = "Mänsklig blåskärms hantering", CourseId = 3 },
                new Subject { SubjectId = 15, SubjectName = "Memorera ett casino", CourseId = 3 }
            );
            return modelBuilder;
        }
    }
}
