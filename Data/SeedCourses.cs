﻿using Microsoft.EntityFrameworkCore;
using School.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Data.Migrations
{
    public class SeedCourses
    {        
        public ModelBuilder AddCourse(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().HasData(
                new Course { CourseId = 1, CourseName = "Programmering.NET" },
                new Course { CourseId = 2, CourseName = "Knep & Knåp" },
                new Course { CourseId = 3, CourseName = "Minnesträning" }
            );
            return modelBuilder;
        }
    }
}
