﻿using Microsoft.EntityFrameworkCore;
using School.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Data
{
    public class SeedTempStored
    {        
        public ModelBuilder AddTempActiveId(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TempStored>().HasData(
                new TempStored {
                    TempStoredId = 1,
                    ActiveCourseId = 1, 
                    ActiveTeacherId = 1 
                }           
            );
            return modelBuilder;
        }
    }
}