﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using School.Data.Migrations;
using School.Models;

namespace School.Data
{
    public class SchoolDbContext : IdentityDbContext
    {
        public SchoolDbContext(DbContextOptions<SchoolDbContext> options) : base(options)
        { }

        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentCourse> StudentsCourses { get; set; }
        public DbSet<TempStored> TempStore { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            SeedStudents seedStudents = new SeedStudents();
            SeedTeachers seedTeachers = new SeedTeachers();
            SeedCourses seedCourses = new SeedCourses();
            SeedSubjects seedSubjects = new SeedSubjects();
            SeedStudentsCourses seedStudentsCourses = new SeedStudentsCourses();
            SeedTempStored seedTempStore = new SeedTempStored();

            modelBuilder.Entity<StudentCourse>()
                .HasKey(sc => new { sc.StudentId, sc.CourseId });

            modelBuilder.Entity<Course>()
                .HasOne(c => c.Teacher)
                .WithOne(t => t.Course)
                .HasForeignKey<Teacher>(t => t.CourseId);

            modelBuilder = seedStudents.AddStudent(modelBuilder);
            modelBuilder = seedTeachers.AddTeacher(modelBuilder);
            modelBuilder = seedCourses.AddCourse(modelBuilder);
            modelBuilder = seedSubjects.AddAssignment(modelBuilder);
            modelBuilder = seedStudentsCourses.AddStudentToCourse(modelBuilder);
            modelBuilder = seedTempStore.AddTempActiveId(modelBuilder);
        }
    }
}