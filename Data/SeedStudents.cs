﻿using Microsoft.EntityFrameworkCore;
using School.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Data
{
    public class SeedStudents
    {
        public ModelBuilder AddStudent(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasData (
                new Student { StudentId = 1, FirstName = "Bjarne", LastName = "Bjarnarsdottir", City = "Reykavik" },
                new Student { StudentId = 2, FirstName = "Chilikon-Arne", LastName = "Jönsson", City = "Helsingborg" },
                new Student { StudentId = 3, FirstName = "Frode", LastName = "Mathiassen", City = "Lindome" },
                new Student { StudentId = 4, FirstName = "Harald", LastName = "Hårfager", City = "Sogn" },
                new Student { StudentId = 5, FirstName = "Hagbard", LastName = "Hårfäste", City = "Bastuträsk" },
                new Student { StudentId = 6, FirstName = "Rabarbro", LastName = "Svensson", City = "Tommelilla" },
                new Student { StudentId = 7, FirstName = "Sten", LastName = "Sture", City = "Birka" },
                new Student { StudentId = 8, FirstName = "Styrbjörn", LastName = "Strularsson", City = "Kautokeino" },
                new Student { StudentId = 9, FirstName = "Tina", LastName = "Törner", City = "Laholm" }
            );
            return modelBuilder;
        }
    }
}
