﻿using Microsoft.EntityFrameworkCore;
using School.Models;

namespace School.Data
{
    public class SeedStudentsCourses
    {
        public ModelBuilder AddStudentToCourse(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentCourse>().HasData(
                new StudentCourse { CourseId = 3, StudentId = 1 },
                new StudentCourse { CourseId = 2, StudentId = 2 },
                new StudentCourse { CourseId = 1, StudentId = 3 },
                new StudentCourse { CourseId = 1, StudentId = 4 },
                new StudentCourse { CourseId = 2, StudentId = 5 },
                new StudentCourse { CourseId = 2, StudentId = 6 },
                new StudentCourse { CourseId = 1, StudentId = 7 },
                new StudentCourse { CourseId = 3, StudentId = 7 },
                new StudentCourse { CourseId = 1, StudentId = 8 },
                new StudentCourse { CourseId = 2, StudentId = 8 },
                new StudentCourse { CourseId = 3, StudentId = 8 },
                new StudentCourse { CourseId = 1, StudentId = 9 },
                new StudentCourse { CourseId = 3, StudentId = 9 }
            );
            return modelBuilder;
        }
    }
}
