﻿using Microsoft.EntityFrameworkCore;
using School.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Data.Migrations
{
    public class SeedTeachers    
    {        
        public ModelBuilder AddTeacher(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teacher>().HasData(
                new Teacher { TeacherId = 1, FirstName = "Al Gore Rithm", LastName = "Clarkson", City = "Wyoming", CourseId = 1 },
                new Teacher { TeacherId = 2, FirstName = "Fråge", LastName = "Fråglund", City = "Lund", CourseId = 2 },
                new Teacher { TeacherId = 3, FirstName = "Professor", LastName = "Baltazar", City = "Ostrava", CourseId = 3 }
            );
            return modelBuilder;
        }
    }
}
