#pragma checksum "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6f19bfe99c6819a5ebe9e3ffa0aba56b78801e7a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_School__CoursesPartial), @"mvc.1.0.view", @"/Views/Shared/School/_CoursesPartial.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Project\C#\Lexicon\School\Views\_ViewImports.cshtml"
using School;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Project\C#\Lexicon\School\Views\_ViewImports.cshtml"
using School.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Project\C#\Lexicon\School\Views\_ViewImports.cshtml"
using School.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6f19bfe99c6819a5ebe9e3ffa0aba56b78801e7a", @"/Views/Shared/School/_CoursesPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6c6a7797b93f705bf89ee21ca88e32bcb55374e3", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_School__CoursesPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DbViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("btn-delete-course"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-sm btn-tr-del"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DeleteCourse", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("btn-delete-teacher"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DeleteTeacher", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("btn-add-teacher-course"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-sm btn-success"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddCourseAndTeacher", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-course", "jsCourseDummy", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-firstName", "jsFirstNameDummy", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-lastName", "jsLastNameDummy", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_11 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-city", "jsCityDummy", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<br />
<div class=""header-courses""><h4>Courses</h4></div>
<div class=""header-teachers""><h4>Teachers</h4></div>

<div id=""course_table"">
    <table id=""table-courses"" class=""table table-sm table-striped"">
        <thead>
            <tr class=""header"">
                <th class=""head col_head_course"">Course</th>
                <th class=""head col_head_del"">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 16 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
             foreach (Course course in Model.Courses)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                 if (course.Teacher != null)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr");
            BeginWriteAttribute("onclick", " onclick=\"", 616, "\"", 738, 3);
            WriteAttributeValue("", 626, "location.href=\'", 626, 15, true);
#nullable restore
#line 20 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
WriteAttributeValue("", 641, Url.ActionLink("DisplayRelationsToCourse", "School", new { idCourse = @course.CourseId }, null), 641, 96, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 737, "\'", 737, 1, true);
            EndWriteAttribute();
            BeginWriteAttribute("id", "\r\n                        id=\"", 739, "\"", 797, 2);
            WriteAttributeValue("", 769, "tr-courseid-", 769, 12, true);
#nullable restore
#line 21 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
WriteAttributeValue("", 781, course.CourseId, 781, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("\r\n                        class=\"tr-course\">\r\n                        <td class=\"col_course\">");
#nullable restore
#line 23 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                          Write(course.CourseName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"col_del p-0\">\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6f19bfe99c6819a5ebe9e3ffa0aba56b78801e7a9633", async() => {
                WriteLiteral("\n                                Del\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-courseId", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 27 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                        WriteLiteral(course.CourseId);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["courseId"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-courseId", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["courseId"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </td>\r\n                    </tr>\r\n");
#nullable restore
#line 32 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 32 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                 
            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        </tbody>
    </table>
    </div>
<div id=""teacher_table"">
    <table id=""table-teachers"" class=""table table-sm table-striped"">
        <thead>
            <tr>
                <th class=""col1_head_teacher"">First Name</th>
                <th class=""col2_head_teacher"">Last Name</th>
                <th class=""col3_head_teacher"">City</th>
                <th class=""head col_head_del"">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 48 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
             foreach (Teacher teacher in Model.Teachers)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 50 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                 if (teacher.Course != null)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr");
            BeginWriteAttribute("onclick", " onclick=\"", 1973, "\"", 2099, 3);
            WriteAttributeValue("", 1983, "location.href=\'", 1983, 15, true);
#nullable restore
#line 52 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
WriteAttributeValue("", 1998, Url.ActionLink("DisplayRelationsToTeacher", "School", new { idTeacher = @teacher.TeacherId }, null), 1998, 100, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2098, "\'", 2098, 1, true);
            EndWriteAttribute();
            BeginWriteAttribute("id", "\r\n                        id=\"", 2100, "\"", 2161, 2);
            WriteAttributeValue("", 2130, "tr-teacherid-", 2130, 13, true);
#nullable restore
#line 53 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
WriteAttributeValue("", 2143, teacher.TeacherId, 2143, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("\r\n                        class=\"tr-teacher\">\r\n                        <td class=\"col1_teacher\">");
#nullable restore
#line 55 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                            Write(teacher.FirstName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"col2_teacher\">");
#nullable restore
#line 56 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                            Write(teacher.LastName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>                    \r\n                        <td class=\"col3_teacher\">");
#nullable restore
#line 57 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                            Write(teacher.City);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"col_del p-0\">\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6f19bfe99c6819a5ebe9e3ffa0aba56b78801e7a15606", async() => {
                WriteLiteral("\n                                Del\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-teacherId", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 61 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                                         WriteLiteral(teacher.TeacherId);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["teacherId"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-teacherId", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["teacherId"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </td>\r\n                    </tr>\r\n");
#nullable restore
#line 66 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 66 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                 
            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        </tbody>
    </table>
    </div>
<div class=""add-remove"">
    <table>
        <tr>
            <td><input id=""input-add-firstname-teacher"" class=""input-add-firstname"" name=""filter"" placeholder=""First name"" type=""text"" /></td>
            <td><input id=""input-add-lastname-teacher"" class=""input-add-col2"" name=""filter"" placeholder=""Last name"" type=""text"" /></td>
            <td><input id=""input-add-city-teacher"" class=""input-add-col3"" name=""filter"" placeholder=""City"" type=""text"" /></td>
        </tr>
        <tr>
            <td><input id=""input-add-course"" class=""input-add-col1"" name=""filter"" placeholder=""Course"" type=""text"" /></td>
        </tr>
        <tr>
            <td>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6f19bfe99c6819a5ebe9e3ffa0aba56b78801e7a19232", async() => {
                WriteLiteral("\n                   Add\n                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-course", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["course"] = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["firstName"] = (string)__tagHelperAttribute_9.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_9);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["lastName"] = (string)__tagHelperAttribute_10.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_10);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["city"] = (string)__tagHelperAttribute_11.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_11);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</div>\r\n<script type=\"text/javascript\">        \r\n    setActiveTeacher(");
#nullable restore
#line 96 "D:\Project\C#\Lexicon\School\Views\Shared\School\_CoursesPartial.cshtml"
                Write(Model.ActiveTeacherId);

#line default
#line hidden
#nullable disable
            WriteLiteral(");\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DbViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
