﻿using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("student_course")]
    public class StudentCourse
    {
        [Column("id_course")]
        public int CourseId { get; set; }

        [Column("course")]
        public Course Course { get; set; }
        
        [Column("id_student")]
        public int StudentId { get; set; }
        
        [Column("student")]
        public Student Student { get; set; }
    }
}