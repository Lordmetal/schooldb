﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("teachers")]
    public class Teacher
    {
        [Column("id_teacher")]
        public int TeacherId { get; set; }

        [Required]
        [Column(name: "first_name")]
        [StringLength(80), DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required]
        [Column("last_name")]
        [StringLength(80), DataType(DataType.Text)]
        public string LastName { get; set; }

        [Required]
        [Column("city")]
        [StringLength(70), DataType(DataType.Text)]
        public string City { get; set; }
        
        [Column("id_courses")]
        public int CourseId { get; set; }
        public Course Course { get; set; }
    }
}