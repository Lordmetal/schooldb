﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("temp_store")]
    public class TempStored
    {
        [Column("id_temp_store")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TempStoredId { get; set; }

        [Column("id_active_course")]
        public int ActiveCourseId { get; set; }

        [Column("id_active_teacher")]
        public int ActiveTeacherId { get; set; }
    }
}