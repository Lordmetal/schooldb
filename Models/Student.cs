﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("student")]
    public class Student
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id_student")]
        public int StudentId { get; set; }

        [Required]
        [Column(name: "first_name")]
        [StringLength(80), DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required]
        [Column("last_name")]
        [StringLength(80), DataType(DataType.Text)]
        public string LastName { get; set; }

        [Required]
        [Column("city")]
        [StringLength(70), DataType(DataType.Text)]
        public string City { get; set; }

        [Column("student_course")]
        public List<Models.StudentCourse> StudentCourses { get; set; } = new List<StudentCourse>();
    }
}