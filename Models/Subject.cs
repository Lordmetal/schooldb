﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("subject")]
    public class Subject
    {
        [Key]
        [Column("id_subject")]
        public int SubjectId { get; set; }
        
        [Required]
        [Column("subject")]
        [StringLength(60), DataType(DataType.Text)]
        public string SubjectName { get; set; }

        [Column("id_course")]
        public int CourseId { get; set; }

        [Column("course")]
        public Course Course { get; set; }
    }
}
