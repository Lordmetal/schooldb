﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Models
{
    [Table("course")]
    public class Course
    {
        [Column("id_course")]
        public int CourseId { get; set; }

        [Required]
        [Column("course")]
        [StringLength(60), DataType(DataType.Text)]
        public string CourseName { get; set; }

        [Column("subject")]
        public List<Subject> Subjects { get; } = new List<Subject>();

        [Column("teacher")]
        public Teacher Teacher { get; set; }

        [Column("id_student_course")]
        public int StudentCourseId { get; set; }
        
        [Column("student_course")]
        public List<StudentCourse> StudentCourses { get; set; } = new List<StudentCourse>();
    }
}